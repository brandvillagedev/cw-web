<?php
/**
 * Admin notice
 *
 * @package WooVina_Extra
 * @category Core
 * @author WooVina
 */

// Exit if accessed directly
if(! defined('ABSPATH')) {
    exit;
}

// The Notice class
if(! class_exists('WooVina_Extra_Admin_Notice')) {

    class WooVina_Extra_Admin_Notice {

        /**
         * Admin constructor
         */
        public function __construct() {
            add_action('admin_notices', array($this, 'admin_notice'));
            add_action('admin_init', array($this, 'dismiss_notice'));
            add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
        }

        /**
         * Display admin notice
         *
         * @since   1.2.6
         */
        public static function admin_notice() {
            // Show notice after 24 hours from installed time.
            if(self::get_installed_time() > strtotime('-24 hours')
                || '1' === get_option('woovina_extra_dismiss_notice')
                || ! current_user_can('manage_options')
                || apply_filters('woovina_show_hooks_notice', false)) {
                return;
            }

            $no_thanks  = wp_nonce_url(add_query_arg('woovina_extra_notice', 'no_thanks_btn'), 'no_thanks_btn');
            $dismiss    = wp_nonce_url(add_query_arg('woovina_extra_notice', 'dismiss_btn'), 'dismiss_btn'); ?>
            
            <div class="notice notice-success woovina-extra-notice">
                <div class="notice-inner">
                    <span class="dashicons dashicons-heart"></span>
                    <div class="notice-content">
                        <p><?php esc_html_e('Thank you for using WooVina, If you’re looking for faster, more in-depth support for WooVina, then our priority support plans are for you. We respond to priority support tickets before basic support tickets. So by purchasing priority support, you can make sure that you get your tickets answered as soon as possible.', 'woovina-extra'); ?></p>
                        <p><a href="https://woovina.com/?ref=dashboard#priority-support" class="btn button-primary" target="_blank"><?php _e('Priority Support', 'woovina-extra'); ?></a><a href="<?php echo $no_thanks; ?>" class="btn button-secondary"><?php _e('No thanks', 'woovina-extra'); ?></a></p>
                    </div>
                    <a href="<?php echo $dismiss; ?>" class="dismiss"><span class="dashicons dashicons-dismiss"></span></a>
                </div>
            </div>
        <?php
        }

        /**
         * Dismiss admin notice
         *
         * @since   1.2.6
         */
        public static function dismiss_notice() {
            if(! isset($_GET['woovina_extra_notice'])) {
                return;
            }

            if('dismiss_btn' === $_GET['woovina_extra_notice']) {
                check_admin_referer('dismiss_btn');
                update_option('woovina_extra_dismiss_notice', '1');
            }

            if('no_thanks_btn' === $_GET['woovina_extra_notice']) {
                check_admin_referer('no_thanks_btn');
                update_option('woovina_extra_dismiss_notice', '1');
            }

            wp_redirect(remove_query_arg('woovina_extra_notice'));
            exit;
        }

        /**
         * Installed time
         *
         * @since   1.2.6
         */
        private static function get_installed_time() {
            $installed_time = get_option('woovina_extra_installed_time');
            if(! $installed_time) {
                $installed_time = time();
                update_option('woovina_extra_installed_time', $installed_time);
            }
            return $installed_time;
        }

        /**
         * Style
         *
         * @since 1.2.1
         */
        public static function admin_scripts() {

            if(self::get_installed_time() > strtotime('-24 hours')
                || '1' === get_option('woovina_extra_dismiss_notice')
                || ! current_user_can('manage_options')
                || apply_filters('woovina_show_hooks_notice', false)) {
                return;
            }

            // CSS
            wp_enqueue_style('we-admin-notice', plugins_url('/assets/css/notice.min.css', __FILE__));

        }

    }

    new WooVina_Extra_Admin_Notice();
}