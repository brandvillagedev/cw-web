<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Dropcap
// **********************************************************************// 

if ( ! function_exists('etheme_dropcap_shortcode') ) :
    function etheme_dropcap_shortcode($atts,$content=null) {
        $a = shortcode_atts( array(
           'style' => '',
           'color' => '',
        ), $atts );

        $style = '';
        if( ! empty( $a['color'] ) ) {
            $style = 'style="color:' . $a['color'] . ';"';
        }
       
        return '<span class="dropcap ' . $a['style'] . '" ' . $style . '>' . $content . '</span>';
    }
endif;