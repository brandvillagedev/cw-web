<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Static Block Shortcode
// **********************************************************************// 
if ( ! function_exists('etheme_block_shortcode') ) :
function etheme_block_shortcode($atts) {
    $a = shortcode_atts(array(
        'class' => '',
        'id' => ''
    ),$atts);

    return etheme_static_block($a['id'], false);
}
endif;