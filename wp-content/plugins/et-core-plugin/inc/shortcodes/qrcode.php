<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! QR Code
// **********************************************************************// 

if ( ! function_exists('etheme_qrcode_shortcode') ) :
    function etheme_qrcode_shortcode($atts, $content = null) {
    $a = shortcode_atts(array(
            'size' => '128',
            'self_link' => 0,
            'title' => 'QR Code',
            'lightbox' => 0,
            'class' => ''
        ), $atts);

        return etheme_qr_code($content,$a['title'],$a['size'],$a['class'],$a['self_link'],$a['lightbox']);
    }
endif;

// **********************************************************************//
// ! QR Code generation
// **********************************************************************//
if(!function_exists('etheme_qr_code')) {
    function etheme_qr_code($text='QR Code', $title = 'QR Code', $size = 128, $class = '', $self_link = false, $lightbox = false ) {
        if( $self_link ) {
            $text = etheme_http();
            if ( $_SERVER['SERVER_PORT'] != '80' ) {
                $text .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
            } else {
                $text .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            }
        }
        $image = 'https://chart.googleapis.com/chart?chs=' . $size . 'x' . $size . '&cht=qr&chld=H|1&chl=' . $text;

        if( $lightbox ) {
            $class .= ' qr-lighbox';
            $output = '<a href="'.$image.'" rel="lightbox" class="'.$class.'"><img src="'.$image.'" /></a>';
        } else{
            $class .= ' qr-image';
            $output = '<img src="'.$image.'"  class="'.$class.'" />';
        }

        return $output;
    }
}