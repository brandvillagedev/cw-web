<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Buttons
// **********************************************************************// 

if ( ! function_exists('etheme_btn_shortcode') ) :
    function etheme_btn_shortcode($atts){
        $a = shortcode_atts( array(
           'title' => 'Button',
           'url' => '#',
           'icon' => '',
           'size' => '',
           'style' => '',
           'et_class' => '',
           'type' => '',
           'target' => ''
       ), $atts );
        $icon = $class = '';
        if($a['icon'] != '') {
            $icon = '<i class="et-icon et-'.$a['icon'].'" style="vertical-align: middle;"></i>';
        }
        if($a['style'] != '') {
            $class .= ' '.$a['style'];
        }
        if($a['type'] != '') {
            $class .= ' '.$a['type'];
        }
        if($a['size'] != '') {
            $class .= ' '.$a['size'];
        }
        if($a['size'] != '') {
            $class .= ' '.$a['size'];
        }
        if($a['et_class'] != '') {
            $class .= ' '.$a['et_class'];
        }
        return '<a target="' . $a['target'] . '" class="btn'. $class .'" href="' . $a['url'] . '"><span>'. $icon . ' ' . $a['title'] . '</span></a>';
    }
endif;