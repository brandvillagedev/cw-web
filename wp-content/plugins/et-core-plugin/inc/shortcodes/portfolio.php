<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');

// **********************************************************************// 
// ! Portfolio
// **********************************************************************// 
if ( !function_exists('etheme_portfolio_shortcode') ) {
    function etheme_portfolio_shortcode($atts, $content) {
        $args = shortcode_atts(array(
            'limit'  => '',
            'columns' => '',
            'ajax' => false
        ), $atts);

        global $et_portfolio_loop;

        $et_portfolio_loop['columns'] = $args['columns'];

        return etheme_portfolio( false, $args['limit'], true );
    }
}

// **********************************************************************// 
// ! Register New Element: Portfolio
// **********************************************************************//
add_action( 'init', 'etheme_register_vc_portfolio');
if(!function_exists('etheme_register_vc_portfolio')) {
    function etheme_register_vc_portfolio() {
        if(!function_exists('vc_map')) return;
        $params = array(
          'name' => '[8THEME] Portfolio',
          'base' => 'portfolio',
          'category' => 'Eight Theme',
          'params' => array(
            array(
              'type' => 'textfield',
              'heading' => esc_html__('Limit', 'xstore'),
              'param_name' => 'limit',
            ),
            array(
              'type' => 'dropdown',
              'heading' => esc_html__('Columns', 'xstore'),
              'param_name' => 'columns',
              'value' => array(
                  '',
                  2,
                  3,
                  4,
                  5,
                  6
              )
          ),
        ),
      );  
      vc_map($params);
    }
}