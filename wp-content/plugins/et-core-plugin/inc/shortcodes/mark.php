<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Mark
// **********************************************************************// 

if ( ! function_exists('etheme_mark_shortcode') ) :
    function etheme_mark_shortcode($atts,$content=null) {
        $a = shortcode_atts( array(
           'style' => '',
           'color' => '',
        ), $atts );

        $style = '';

        if( ! empty( $a['color'] ) ) {
            $style = 'style="background-color:' . $a['color'] . ';"';
        }

        if( ! empty( $a['color'] ) && $a['style'] == 'paragraph' ) {
            $style = 'style="color:' . $a['color'] . ';"';
        }
       
        return '<span class="mark-text ' . $a['style'] . '" ' . $style . '>' . $content . '</span>';
    }
endif;