<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Tooltip
// **********************************************************************// 

if ( ! function_exists('etheme_tooltip_shortcode') ) :
    function etheme_tooltip_shortcode($atts,$content=null){
        $a = shortcode_atts( array(
           'position' => 'top',
           'text' => '',
           'class' => ''
       ), $atts );
       
        return '<div class="et-tooltip '.$a['class'].'" rel="tooltip" data-placement="'.$a['position'].'" data-original-title="'.$a['text'].'"><div><div>'.$content.'</div></div></div>';
    }
endif;