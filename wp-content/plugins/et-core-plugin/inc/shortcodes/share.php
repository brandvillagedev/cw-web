<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');
// **********************************************************************// 
// ! Share This Product
// **********************************************************************// 

if ( ! function_exists('etheme_share_shortcode') ) :
    function etheme_share_shortcode($atts, $content = null) {
        $socials = etheme_get_option('socials');
        $socials = $socials['enabled'];
        extract(shortcode_atts(array(
            'title'  => '',
            'text' => '',
            'tooltip' => 1,
            'twitter' => array_key_exists( 'share_twitter', $socials ),
            'facebook' =>  array_key_exists( 'share_facebook', $socials ),
            'vk' =>  array_key_exists( 'share_vk', $socials ),
            'pinterest' =>  array_key_exists( 'share_pinterest', $socials ),
            'mail' =>  array_key_exists( 'share_mail', $socials ),
            'linkedin' =>  array_key_exists( 'share_linkedin', $socials ),
            'whatsapp' =>  array_key_exists( 'share_whatsapp', $socials ),
            'skype' =>  array_key_exists( 'share_skype', $socials ),
            'class' => ''
        ), $atts));
        global $post;
        if(!isset($post->ID)) return;
        $html = '';
        $permalink = get_permalink($post->ID);
        $tooltip_class = '';
        if($tooltip) {
            $tooltip_class = 'title-toolip';
        }
        $image =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
        $image = $image[0];
        $post_title = rawurlencode(get_the_title($post->ID));
        $post_title = ( ! empty( $text ) ) ? $text : $post_title;
        if($title) $html .= '<span class="share-title">'.$title.'</span>';
        $html .= '
            <ul class="menu-social-icons '.$class.'">
        ';
        if($twitter == 1) {
            $html .= '
                    <li>
                        <a href="https://twitter.com/share?url='.$permalink.'&text='.$post_title.'" class="'.$tooltip_class.'" title="'.__('Twitter', 'xstore').'" target="_blank">
                            <i class="et-icon et-twitter"></i>
                        </a>
                    </li>
            ';
        }

        if($facebook == 1) {
            $html .= '
                    <li>
                        <a href="http://www.facebook.com/sharer.php?u='.$permalink.'&amp;images='.$image.'" class="'.$tooltip_class.'" title="'.__('Facebook', 'xstore').'" target="_blank">
                            <i class="et-icon et-facebook"></i>
                        </a>
                    </li>
            ';
        }

        if($vk == 1) {
            $html .= '
                    <li>
                        <a href="http://vk.com/share.php?url='.$permalink.'&image='.$image.'?&title='.$post_title.'" class="'.$tooltip_class.'" title="'.__('VK', 'xstore').'" target="_blank">
                            <i class="et-icon et-vk"></i>
                        </a>
                    </li>
            ';
        }

        if($pinterest == 1) {
            $html .= '
                    <li>
                        <a href="http://pinterest.com/pin/create/button/?url='.$permalink.'&amp;media='.$image.'&amp;description='.$post_title.'" class="'.$tooltip_class.'" title="'.__('Pinterest', 'xstore').'" target="_blank">
                            <i class="et-icon et-pinterest"></i>
                        </a>
                    </li>
            ';
        }

        if($mail == 1) {
            $html .= '
                    <li>
                        <a href="mailto:enteryour@addresshere.com?subject='.$post_title.'&amp;body='. __('Check%20this%20out:%20', 'xstore' ) .$permalink.'" class="'.$tooltip_class.'" title="'.__('Mail to friend', 'xstore').'" target="_blank">
                            <i class="et-icon et-message"></i>
                        </a>
                    </li>
            ';
        }

        if($linkedin == 1) {
            $html .= '
                    <li>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url='.$permalink.'&title='.$text.'" class="'.$tooltip_class.'" title="'.__('linkedin', 'xstore').'" target="_blank">
                            <i class="et-icon et-linkedin"></i>
                        </a>
                    </li>
            ';
        }

        if($whatsapp == 1) {
            $html .= '
                    <li>
                        <a href="whatsapp://send?text='.$permalink.'" class="'.$tooltip_class.'" title="'.__('whatsapp', 'xstore').'" target="_blank">
                            <i class="et-icon et-whatsapp"></i>
                        </a>
                    </li>
            ';
        }

        if($skype == 1) {
            $html .= '
                    <li>
                        <a href="https://web.skype.com/share?url='.$permalink.'" title="'.__('skype', 'xstore').'" target="_blank">
                            <i class="et-icon et-skype"></i>
                        </a>
                    </li>
            ';
        }
        
        $html .= '
            </ul>
        ';
        return $html;
    }
endif;