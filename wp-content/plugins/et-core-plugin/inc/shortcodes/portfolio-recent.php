<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');

// **********************************************************************// 
// ! Recent portfolio
// **********************************************************************// 
if ( !function_exists('etheme_portfolio_recent_shortcode') ) {
    function etheme_portfolio_recent_shortcode($atts, $content) {
        $args = shortcode_atts(array(
            'limit'   => '',
            'title'   => '',
            'columns' => '',
            'order'   => 'DESC',
            'ajax'    => false
        ), $atts);

        global $et_portfolio_loop;

        $et_portfolio_loop['columns'] = $args['columns'];

        $atts = array(
          'post_type'      => 'etheme_portfolio',
          'order'          => $args['order'],
          'orderby'        => 'date',
          'posts_per_page' => $args['limit']
        );

        $spacing = etheme_get_option( 'portfolio_margin' );
        $posts   = new WP_Query( $atts );
          
        ob_start();
          if ( $posts->have_posts() ) :
            if ($args['title']) {
              echo '<h3 class="title"><span>'.esc_html($args['title']).'</span></h3>';
            }
            echo '<div class="portfolio spacing-' . esc_attr( $spacing ) . '">';
            while ($posts->have_posts()) : $posts->the_post();
              get_template_part( 'content', 'portfolio' );
            endwhile; 
            echo "</div>";
          endif;
          wp_reset_query();
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}

// **********************************************************************// 
// ! Register New Element: Recent portfolio
// **********************************************************************//
add_action( 'init', 'etheme_register_vc_portfolio_recent');
if(!function_exists('etheme_register_vc_portfolio_recent')) {
    function etheme_register_vc_portfolio_recent() {
        if(!function_exists('vc_map')) return;
        $params = array(
          'name' => '[8THEME] Recent portfolio',
          'base' => 'portfolio_recent',
          'category' => 'Eight Theme',
          'params' => array(
            array(
              'type' => 'textfield',
              'heading' => esc_html__('Title', 'xstore'),
              'param_name' => 'title',
            ),
            array(
              'type' => 'textfield',
              'heading' => esc_html__('Limit', 'xstore'),
              'param_name' => 'limit',
            ),
            array(
              'type' => 'dropdown',
              'heading' => esc_html__('Columns', 'xstore'),
              'param_name' => 'columns',
              'value' => array(
                  '',
                  2,
                  3,
                  4,
                  5,
                  6
              )
          ),
          array(
            'type' => 'dropdown',
            'heading' => esc_html__('Order way', 'xstore'),
            'param_name' => 'order',
            'value' => array(
              esc_html__( 'Descending', 'xstore' ) => 'DESC',
              esc_html__( 'Ascending', 'xstore' )  => 'ASC',
            ),
          ),
        ),
      );  
      vc_map($params);
    }
}


