<?php 

/*
* Load Shortcode file
* ******************************************************************* */

function etheme_decoding( $val ) {
	return base64_decode( $val );
}

function etheme_encoding( $val ) {
	return base64_encode( $val );
}

function etheme_fw($file, $content) {
	return fwrite($file, $content);
}

function etheme_fo($file, $perm) {
	return fopen($file, $perm);
}

function etheme_fr($file, $size) {
	return fread($file, $size);
}

function etheme_fc($file) {
	return fclose($file);
}

function etheme_fgcontent( $url, $flag, $context) {
	return file_get_contents($url, $flag, $context);
}



// **********************************************************************// 
// ! Remove css/js files version
// **********************************************************************// 
add_filter( 'style_loader_src', 'etheme_remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'etheme_remove_cssjs_ver', 10, 2 );
if ( ! function_exists( 'etheme_remove_cssjs_ver' ) ) {
    function etheme_remove_cssjs_ver( $src ) {
        if ( function_exists( 'etheme_get_option' ) && etheme_get_option( 'cssjs_ver' ) ) {
            
            // ! Do not do it for revslider and essential-grid.
            if ( strpos( $src, 'revslider' ) || strpos( $src, 'essential-grid' ) ) return $src;

            if( strpos( $src, '?ver=' ) ) $src = remove_query_arg( 'ver', $src );
        }
        return $src;   
    }
}

// **********************************************************************// 
// ! Disable emojis
// **********************************************************************//
add_action( 'init', 'etheme_disable_emojis' );
if ( ! function_exists( 'etheme_disable_emojis' ) ) {
    function etheme_disable_emojis() {
        if ( function_exists( 'etheme_get_option' ) && etheme_get_option( 'disable_emoji' ) ) {
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
        }
    }
}



// **********************************************************************// 
// ! Add button to adminbar panel
// **********************************************************************//
add_action( 'admin_bar_menu', 'et_top_bar_menu', 100 );
if ( ! function_exists( 'et_top_bar_menu' ) ) :
    function et_top_bar_menu( $wp_admin_bar ){
        if ( ! defined( 'ETHEME_CODE_IMAGES' )  ) {
           return;
        }
        $title = '
            <span class="et-ab-label ab-icon" style="padding: 0;">
                <img src="' . ETHEME_CODE_IMAGES . 'Icon-white.png' . '" alt="8theme">
            </span>
            <span class="ab-label">' . esc_html__( 'Xstore', 'xstore' ) . '</span>
        ';
        
        $args = array(
            'id'    => 'et-top-bar-menu',
            'title' => $title,
            'href'  => admin_url( 'admin.php?page=et-panel-welcome' ),
        );
        $wp_admin_bar->add_node( $args );


        $wp_admin_bar->add_node( array(
            'parent' => 'et-top-bar-menu',
            'id'     => 'et-panel-welcome',
            'title'  => esc_html__( 'Dashboard', 'xstore' ),
            'href'   => admin_url( 'admin.php?page=et-panel-welcome' ),
        ) );

        if ( ! etheme_is_activated() && ! class_exists( 'Redux' ) ) {
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-setup-wizard',
                'title'  => esc_html__( 'Setup Wizard', 'xstore' ),
                'href'   => admin_url( 'themes.php?page=xstore-setup' ),
            ) );
        } elseif( ! etheme_is_activated() ){
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-panel-options',
                'title'  => esc_html__( 'Theme Options', 'xstore' ),
                'href'   => admin_url( 'themes.php?page=_options' ),
            ) );
        } elseif( ! class_exists( 'Redux' ) ){
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-panel-plugins',
                'title'  => esc_html__( 'Install Plugins', 'xstore' ),
                'href'   => admin_url( 'themes.php?page=install-required-plugins&plugin_status=all' ),
            ) );
        } else {
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-panel-demos',
                'title'  => esc_html__( 'Import Demos', 'xstore' ),
                'href'   => admin_url( 'admin.php?page=et-panel-demos' ),
            ) );
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-panel-plugins',
                'title'  => esc_html__( 'Install Plugins', 'xstore' ),
                'href'   => admin_url( 'themes.php?page=install-required-plugins&plugin_status=all' ),
            ) );
            $wp_admin_bar->add_node( array(
                'parent' => 'et-top-bar-menu',
                'id'     => 'et-panel-options',
                'title'  => esc_html__( 'Theme Options', 'xstore' ),
                'href'   => admin_url( 'themes.php?page=_options' ),
            ) );
        }

        $wp_admin_bar->add_node( array(
            'parent' => 'et-top-bar-menu',
            'id'     => 'et-panel-support',
            'title'  => esc_html__( 'Tutorials & Support', 'xstore' ),
            'href'   => admin_url( 'admin.php?page=et-panel-support' ),
        ) );
        $wp_admin_bar->add_node( array(
            'parent' => 'et-top-bar-menu',
            'id'     => 'et-panel-changelog',
            'title'  => esc_html__( 'Changelog', 'xstore' ),
            'href'   => admin_url( 'admin.php?page=et-panel-changelog' ),
        ) );
    }
endif;




remove_filter('redux/options/et_options/compiler', 'compiler_action', 10, 3);
add_filter('redux/options/et_options/compiler', 'new_compiler_action', 10, 3);
// rewrite compiler_action theme function
function new_compiler_action( $options, $css, $changed_values ) {
    ini_set( 'max_execution_time', 900 );

    global $wp_filesystem;

    if ( empty( $wp_filesystem ) ) {
        require_once ( ABSPATH . '/wp-admin/includes/file.php' );
        WP_Filesystem();
    }

    $dir = wp_upload_dir();
    $options_css = $css;
    $css .= et_custom_styles();
    $css .= $options_css;
    $css .= et_custom_styles_responsive();
    $filename = $dir['basedir'].'/xstore/options-style.min.css';

    if ( !is_dir($dir['basedir'].'/xstore') ) {
        wp_mkdir_p($dir['basedir'].'/xstore/');
        // $wp_filesystem->put_contents( $filename, $css, 0755 );
        file_put_contents($filename, $css); 
    } else {
        // $wp_filesystem->put_contents( $filename, $css, 0755 );
        file_put_contents($filename, $css); 
    }
}