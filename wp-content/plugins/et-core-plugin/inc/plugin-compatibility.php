<?php  defined('ABSPATH') || exit( 'No direct script access allowed' );
/**
 * Check plugin compatibility
 *
 * @since   1.2.1
 * @version 1.0.0
 */
function xstore_plugin_compatibility(){
	$plugins = array();

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( is_plugin_active( 'legenda-core/legenda-core.php' ) ) {
	    $plugins[] = 'Legenda core';
	} 

	if ( is_plugin_active( 'woopress-core/woopress-core.php' ) ) {
	    $plugins[] = 'Woopress core';
	} 

	if ( is_plugin_active( 'classico-core-plugin/post-types.php' ) ) {
	    $plugins[] = 'Classico core';
	} 

	if ( is_plugin_active( 'royal-core/royal-core.php' ) ) {
	    $plugins[] = 'Royal core';
	} 

	if ( count( $plugins ) ) {
	    $html = '<div class="error">';
	        $html .= '<p>' . esc_html__( 'Attention!', 'xstore-core' ) . '</p>';
	        $html .= '<p>';
	            $html .= '<strong>';
	                $html .= '<span>' . esc_html__( 'Xstore Core plugin conflicts with the following plugins: ', 'xstore-core' ) . '</span>';
	                $_i = 0;
	                foreach ( $plugins as $value ) {
	                    $_i++;
	                    if ( $_i == count( $plugins ) ) {
	                        $html .= '<span>' . $value . '</span>.';
	                    } else {
	                        $html .= '<span>' . $value . '</span>, ';
	                    }
	                }
	            $html .= '</strong>';
	        $html .= '</p>';
	        $html .= '<p>' . esc_html__( 'Keep enabled only plugin that comes bundled with activated theme.', 'xstore-core' ) . '</p>';
	    $html .= '</div>';

	    add_filter( 'admin_notices', function($msg) use ($html){ echo $html; } );

	    return false;
	}

	return true;
}